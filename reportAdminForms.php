<?php
    // Load comments from database
    $sql = "SELECT comments.id, acgmembers.callsign, comments.commentdate, comments.commenttext ".
           "FROM comments LEFT JOIN acgmembers ON comments.commentauthorid = acgmembers.id ".
           "WHERE reportid = $report_id";
    $comment_result = mysqli_query($dbx, $sql);
    
    if($accepted){
        $sql = "SELECT callsign FROM acgmembers ".
               "RIGHT JOIN reports ON reports.acceptedby = acgmembers.id ".
               "WHERE reports.id = $report_id";
        $query = mysqli_query($dbx, $sql);        
        $racceptance_result = mysqli_fetch_assoc($query);
        $acceptedBy = $racceptance_result["callsign"];
    }
?>

<div>
    <span class="AARSpanLeft">Report accepted by:</span>
    <span class="AARSpanRight"><?php if($accepted){echo($acceptedBy);} ?></span>
    <hr>
</div>

<?php
    while($row = mysqli_fetch_assoc($comment_result)) { 
?>
    <div>
        <span class="AARSpanLeft">Comment by <?php echo $row["callsign"];?></span>
        <?php 
            if(isset($_SESSION["admin"])){
                if($_SESSION["admin"]){
        ?>
            <button id="deleteBtn" onclick="deleteComment(<?php echo $row["id"];?>)">X</button>
                    
        <?php            
                }
            }
        ?>
        <p><?php echo parseImageTags($row["commenttext"]);?></p>
    <hr>
    </div>

<?php } 
// check if user is admin. If yes, show acceptance button and comment input.
if(isset($_SESSION["admin"])){
    if($_SESSION["admin"]){
?>        
<form id="afterActionReportCommentRAF" onsubmit="return false;" >
    
    <div>
        Comment:<br>
        <textarea id="comment" rows="5" cols="50"></textarea>
    </div>
    
    <button id="submitBtn" onclick="submitComment(<?php echo($report_id.",".$user_id);?>)">Submit Comment</button>
    <div id="submitStatus">&nbsp;</div>
    <div>
        <button id="acceptStatusChange" onclick="changeAccepted(<?php echo($accepted.",".$report_id.",".$user_id.",".$m_id);?>)">
            <?php if($accepted) {echo "Revoke Report Acceptance";} else {echo "Accept Report";} ?></button>
    </div>
</form>    
<?php 
        }
    }
