<?php
    include(dirname(__FILE__).'/header0.php');
    
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    $dbx = getDBx();
    
    if(filter_has_var(INPUT_GET, "r_id")){
        $report_id = filter_input(INPUT_GET, "r_id");
    } else {
        header("location: missionList.php");
        exit();
    }
       
    $user_id = $_SESSION["userID"];
    //Load this report
    
    $sql = "SELECT swarmslw.name AS swarm, swarmpos.name AS swarmpos, ".
           "careercharacters.firstname, careercharacters.lastname, acgmembers.callsign, ".
           "acgmembers.id AS m_id, ".
           "reports.aerodrome, reports.markings, reports.synopsis, reports.dateSubmitted, ".
           "aeroplanes.name AS aeroplane, ".
           "pilotstatus.status AS pilotstatus, aeroplanestatus.status AS aeroplanestatus, ".
           "reports.accepted ".
           "FROM reports ".
           "LEFT JOIN reportdetailslw ON reportdetailslw.reportid = reports.id ".
           "LEFT JOIN swarmslw ON swarmslw.id = reportdetailslw.swarm ".
           "LEFT JOIN swarmpos ON swarmpos.id = reportdetailslw.swarmpos ". 
           "LEFT JOIN careercharacters ON reports.authorid = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
           "LEFT JOIN aeroplanes ON aeroplanes.id = reports.aeroplane ".
           "LEFT JOIN pilotstatus ON pilotstatus.id = reports.pilotstatus ".
           "LEFT JOIN aeroplanestatus ON aeroplanestatus.id = reports.aeroplanestatus ".
           "WHERE reports.id = $report_id";
    
    $query = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($query)>0) {
        
        $r_result = mysqli_fetch_assoc($query);
        
        $swarm = $r_result["swarm"];
        $swarmPos = $r_result["swarmpos"];
        
        $firstName = $r_result["firstname"];
        $lastName = $r_result["lastname"];
        $callsign = $r_result["callsign"];
        $m_id = $r_result["m_id"];
        $m_link = "memberDetails.php?m_id=".$m_id;
        
        $aerodrome = $r_result["aerodrome"];
        $aeroplane = $r_result["aeroplane"];
        $markings = $r_result["markings"];
        $synopsis = parseImageTags($r_result["synopsis"]);
        
        $pilotStatus = $r_result["pilotstatus"];
        $aeroplaneStatus = $r_result["aeroplanestatus"];
        
        $dateSubmitted = $r_result["dateSubmitted"];
        $accepted = $r_result["accepted"];
                
    } else {
       
        //TODO: REDIRECT SCRIPT!
//        header("location: missionList.php");
//        exit();
    }
       
    // Load claims from database and add them to the form
    $sql = "SELECT claimslw.id, aeroplanes.name, claimslw.claimtime, claimslw.accepted, ".
           "claimslw.confirmed, claimslw.witness AS witnessid, acgmembers.callsign AS witnesscallsign, ".
           "fateofcrew.type AS fateofcrew, claimslw.opponent, claimslw.place, ".
           "typeofdestr.type AS typeofdestr, typeofimpact.type AS typeofimpact ".
           "FROM claimslw ".
           "RIGHT JOIN reports ON reports.id = claimslw.reportid ".
           "LEFT JOIN aeroplanes ON aeroplanes.id = claimslw.aeroplane ".
           "LEFT JOIN fateofcrew ON fateofcrew.id = claimslw.fateofcrew ".
           "LEFT JOIN typeofdestr ON typeofdestr.id = claimslw.typeofdestr ".
           "LEFT JOIN typeofimpact ON typeofimpact.id = claimslw.typeofimpact ".
           "LEFT JOIN acgmembers ON acgmembers.id = claimslw.witness ".
           "WHERE reports.id = $report_id";
//    echo $sql;
    $claim_result = mysqli_query($dbx, $sql);
    
    // Load ground claims from database and add them to the form
    $sql = "SELECT groundtargets.name, claimsground.id, claimsground.amount, ".
           "claimsground.description, claimsground.accepted ". 
           "FROM claimsground ".
           "RIGHT JOIN reports ON reports.id = claimsground.reportid ".
           "LEFT JOIN groundtargets ON groundtargets.id = claimsground.object ".
           "WHERE reports.id = $report_id";
//    echo $sql;
    $groundclaim_result = mysqli_query($dbx, $sql);
    
    // Load last rank
    $sql = "SELECT ranks.name ".
           "FROM ranks ".
           "RIGHT JOIN promotions ON (ranks.faction, ranks.value) = ('LW', promotions.value) ".
           "JOIN ".
                "(SELECT promotions.memberid, promotions.date, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                "FROM promotions WHERE date <= '$mi_realDate' GROUP BY memberid) AS lastPromotionDates ".
           "ON (lastPromotionDates.memberid, lastPromotionDates.pdate) = 
                (promotions.memberid, UNIX_TIMESTAMP(promotions.date)) ".
           "WHERE promotions.memberid = $m_id";
    $rank_querry = mysqli_query($dbx, $sql);
    $rank_result = mysqli_fetch_assoc($rank_querry);
    $rank = $rank_result["name"];
    
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<style>
    .claimLW {border-bottom: 1px solid black;}
</style>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<script src="jscript/reportAdminLogic.js"></script>

<?php include(dirname(__FILE__).'/reportMenu.php'); ?> 

<p class="form_id">ACG-PAM/300-210.1</p>
<h3>After Action Report:</h3>
<div>
    <span class="AARSpanLeft">Date:</span>
    <span class="AARSpanRight"><?php echo $dateSubmitted; ?></span>
</div>

<div>
    <span class="AARSpanLeft">Name:</span>
    <span class="AARSpanRight"><a href="<?php echo($m_link);?>"><?php echo $firstName." '".$callsign."' ".$lastName; ?></a></span>
</div>

<div>
    <span class="AARSpanLeft">Rank:</span>
    <span class="AARSpanRight"><?php echo $rank; ?></span>
</div>

<div>    
    <span class="AARSpanLeft">Unit:</span>
    <span class="AARSpanRight"><?php echo(nl2br($squadron_name)); ?></span>
</div>

<div>
    <span class="AARSpanFlightLeft">Swarm:</span>
    <span class="AARSpanFlightRight"><?php echo($swarm); ?></span>

    <span class="AARSpanFlightPosLeft">Swarm Pos.:</span>
    <span class="AARSpanFlightPosRight"><?php echo($swarmPos); ?></span>
</div> 

<div>
    <span class="AARSpanLeft">Type:</span>
    <span class="AARSpanRight"><?php echo($aeroplane); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Markings:</span>
    <span class="AARSpanRight"><?php echo $markings ?></span>
</div>

<div>
    <span class="AARSpanLeft">Aerodrome:</span>
    <span class="AARSpanRight"><?php echo $aerodrome ?></span>
</div>

<div>
    <span class="AARSpanLeft">Pilot status:</span>
    <span class="AARSpanRight"><?php echo($pilotStatus); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Aircraft status:</span>
    <span class="AARSpanRight"><?php echo($aeroplaneStatus); ?></span>
</div>
<br>
<div id="claims">Claims:<br>
<?php
$isAdmin = false;
if(isset($_SESSION["admin"])){
    if($_SESSION["admin"]){
        $isAdmin = true;
    }
}

while($r_row = mysqli_fetch_assoc($claim_result)) {
    if($r_row["id"]!=null) {
        
        $claim_id = $r_row["id"];
        $confirmed = $r_row["confirmed"];
        $claim_accepted = $r_row["accepted"];
        $witness_callsign = $r_row["witnesscallsign"];
        $groundConf = ($r_row["witnessid"] == -1);
        if($groundConf){
            $witness_callsign = "Ground Unit";
        }
?>
    <hr>
    <table>
    <tr><td>Time:</td><td>Place:</td><td>Aircraft:</td><td>Markings:</td>
    <td></td>
    </tr>
    <tr><td class="claimLW"><?php echo($r_row["claimtime"]); ?></td><td class="claimLW"><?php echo($r_row["place"]); ?></td>
        <td class="claimLW"><?php echo($r_row["name"]); ?></td><td class="claimLW"><?php echo($r_row["opponent"]); ?></td></tr>
    <tr><td>Type of destruction:</td><td>Type of impact on the ground:</td><td>Fate of crew:</td>
        <td>Witness:</td><td>Confirmed:</td></tr>
    <tr><td class="claimLW"><?php echo($r_row["typeofdestr"]); ?></td><td class="claimLW"><?php echo($r_row["typeofimpact"]); ?></td>
        <td class="claimLW"><?php echo($r_row["fateofcrew"]); ?></td>
        <td class="claimLW"><?php echo($witness_callsign); ?></td>
        <?php if($r_row["witnessid"]==$_SESSION["userID"]){ ?>
        <td>
            <button id="confirmClaim" onclick="confirmClaim(<?php echo($confirmed.",".$r_row["id"]);?>, 'LW')">
            <?php if($confirmed) {echo "Revoke confirmation";} else {echo "Confirm";} ?></button>
        </td>
        <?php } else if($groundConf && $isAdmin){ ?>
        <td>
            <button id="confirmClaim" onclick="confirmClaim(<?php echo($confirmed.",".$r_row["id"]);?>, 'LW')">
            <?php if($confirmed) {echo "Revoke confirmation";} else {echo "Confirm";} ?></button>
        </td>
        <?php } else { ?>
        <td class="claimLW"><?php if($confirmed) {echo "Confirmed";} else {echo "";} ?></td>
        <?php } ?>
    </tr>
        
    </table>
    <br>
    <?php } } ?>
</div>
<div id="claims">Ground claims:<br>
    <hr>
    <table>
    <?php 
        while($r_row = mysqli_fetch_assoc($groundclaim_result)) {
        
        $claim_id = $r_row["id"];
        $claim_accepted = $r_row["accepted"];
        if(!is_null($claim_id)){
    ?>
    <tr>
        <td><?php echo($r_row["name"]);?></td>
        <td><?php echo("Amount: ".$r_row["amount"]);?></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3"><?php echo($r_row["description"]);?></td>
    </tr>
<?php
        }
    }
?>
    </table>
</div>
<div>
    <span class="AARSpanLeft">Synopsis (Optional):</span>
    <p><?php echo(nl2br($synopsis)) ?></p>
    <hr>
</div>
    
<?php include(dirname(__FILE__).'/footer.php');