<?php
    include(dirname(__FILE__).'/header0.php');
    
    // Setting up indices to spread content over several pages.
    if(filter_has_var(INPUT_GET, "index")) {
        $indexLetter = filter_input(INPUT_GET, "index").'%';
    } else {
        $indexLetter = '%';
    }
    $indexClause = "lastName LIKE '$indexLetter' ";
    $indexPost = "index=$indexLetter";
    if(filter_has_var(INPUT_GET, "sqn")) {
        $sqn = filter_input(INPUT_GET, "sqn");
        $sqnClause = "AND squadrons.id = $sqn ";
        $sqnPost = "sqn=$sqn";
    } else {
        $sqnClause = "";
        $sqnPost = "";
    }
    
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $n_entries = 18;
    $start_from = ($page - 1)*$n_entries;
    $dbx = getDBx();
    
    $sql =  "SELECT careercharacters.id, careercharacters.firstName, careercharacters.lastName, ".
            "acgmembers.callsign, squadrons.name AS sname, ranks.abreviation, characterstatus.status, ".
            "lastMissionHDate.hdate ".
            "FROM careercharacters ".
            "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
            "LEFT JOIN characterstatus ON careercharacters.characterStatus = characterstatus.id ".
            "LEFT JOIN ".
                "(SELECT reports.authorID, MAX(missions.histDate) AS hdate FROM reports ".
                "LEFT JOIN missions ON reports.missionID = missions.id GROUP BY reports.authorID ".
                ") AS lastMissionHDate ".
            "ON careercharacters.id = lastMissionHDate.authorID ".
            "LEFT JOIN ".
               "(SELECT careercharacters.id, transfers.squadronID FROM careercharacters ".
               "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
               "LEFT JOIN transfers ON acgmembers.id = transfers.memberID ".
               "JOIN ".
                    
                    "(SELECT reports.authorID, MAX(characterTransfers.transferDate) AS tdate FROM reports ".
                    "LEFT JOIN missions ON reports.missionID = missions.id ".
                    "JOIN ".
                        
                        "(SELECT reports.authorID, MAX(missions.histDate) AS hdate FROM reports ".
                        "LEFT JOIN missions ON reports.missionID = missions.id GROUP BY reports.authorID ".
                        ") AS lastMissionHDate ".
                    
                    "ON (reports.authorID, missions.histDate) = (lastMissionHDate.authorID, lastMissionHDate.hdate) ".
                    "LEFT JOIN ".
                        
                        "(SELECT careercharacters.id, transfers.transferDate FROM careercharacters ".
                        "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
                        "LEFT JOIN transfers ON acgmembers.id = transfers.memberID ".
                        ")AS characterTransfers ".
                    
                    "ON (reports.authorID = characterTransfers.id) AND (missions.realDate >= characterTransfers.transferDate) ".
                    "GROUP BY reports.authorID ".
                    ") AS lastTransferDate ".
            
                "ON (lastTransferDate.authorID, lastTransferDate.tdate) = (careercharacters.id, transfers.transferDate) ".
                ") AS currentSquadron ".
            "ON careercharacters.id = currentSquadron.id ".
            "LEFT JOIN squadrons ON currentSquadron.squadronID = squadrons.id ".
            "LEFT JOIN ".
               "(SELECT careercharacters.id, promotions.value FROM careercharacters ".
               "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
               "LEFT JOIN promotions ON acgmembers.id = promotions.memberID ".
               "JOIN ".
                    
                    "(SELECT reports.authorID, MAX(characterPromotions.date) AS pdate FROM reports ".
                    "LEFT JOIN missions ON reports.missionID = missions.id ".
                    "JOIN ".
                        
                        "(SELECT reports.authorID, MAX(missions.histDate) AS hdate FROM reports ".
                        "LEFT JOIN missions ON reports.missionID = missions.id GROUP BY reports.authorID ".
                        ") AS lastMissionHDate ".
                    
                    "ON (reports.authorID, missions.histDate) = (lastMissionHDate.authorID, lastMissionHDate.hdate) ".
                    "LEFT JOIN ".
                        
                        "(SELECT careercharacters.id, promotions.date FROM careercharacters ".
                        "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
                        "LEFT JOIN promotions ON acgmembers.id = promotions.memberID ".
                        ")AS characterPromotions ".
                    
                    "ON (reports.authorID = characterPromotions.id) AND (missions.realDate >= characterPromotions.date) ".
                    "GROUP BY reports.authorID ".
                    ") AS lastPromotionDate ".
            
                "ON (lastPromotionDate.authorID, lastPromotionDate.pdate) = (careercharacters.id, promotions.date) ".
                ") AS currentRank ".
            "ON careercharacters.id = currentRank.id ".
            "LEFT JOIN ranks ON (currentRank.value, squadrons.faction) = (ranks.value, ranks.faction) ".
            "WHERE $indexClause $sqnClause ORDER BY lastName ASC LIMIT $start_from, $n_entries";
//    echo $sql;
    $result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(careercharacters.id) FROM careercharacters ".
           "LEFT JOIN ".
               "(SELECT careercharacters.id, transfers.squadronID FROM careercharacters ".
               "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
               "LEFT JOIN transfers ON acgmembers.id = transfers.memberID ".
               "JOIN ".
                    
                    "(SELECT reports.authorID, MAX(characterTransfers.transferDate) AS tdate FROM reports ".
                    "LEFT JOIN missions ON reports.missionID = missions.id ".
                    "JOIN ".
                        
                        "(SELECT reports.authorID, MAX(missions.histDate) AS hdate FROM reports ".
                        "LEFT JOIN missions ON reports.missionID = missions.id GROUP BY reports.authorID ".
                        ") AS lastMissionHDate ".
                    
                    "ON (reports.authorID, missions.histDate) = (lastMissionHDate.authorID, lastMissionHDate.hdate) ".
                    "LEFT JOIN ".
                        
                        "(SELECT careercharacters.id, transfers.transferDate FROM careercharacters ".
                        "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
                        "LEFT JOIN transfers ON acgmembers.id = transfers.memberID ".
                        ")AS characterTransfers ".
                    
                    "ON (reports.authorID = characterTransfers.id) AND (missions.realDate >= characterTransfers.transferDate) ".
                    "GROUP BY reports.authorID ".
                    ") AS lastTransferDate ".
            
                "ON (lastTransferDate.authorID, lastTransferDate.tdate) = (careercharacters.id, transfers.transferDate) ".
                ") AS currentSquadron ".
            "ON careercharacters.id = currentSquadron.id ".
            "LEFT JOIN squadrons ON currentSquadron.squadronID = squadrons.id ".
            "WHERE $indexClause $sqnClause";
//    echo $sql;
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/memberMenu.php'); ?> 
<p class="form_id">ACG-PAM/200-001.1</p>
<h3>ACG-Members:</h3>
<div>
    <p>These pages list all ACG characters with their corresponding names, 
    callsigns, their status and their last active squadron. Click on any character
    to access a detailed profile.</p>
</div>
<div class="indexSelect">
    <?php createPageIndex($indexLetter, "characterList.php?".$sqnPost."&"); ?>
</div>
<div>
    <table>
        <thead>
            <tr>
                <th>Rank:</th>
                <th>Name:</th>
                <th>Callsign:</th>
                <th>Status:</th>
                <th>Squadron/Staffel:</th>
                <th>Last mission:</th>
            </tr>
        </thead>
        <?php
            while($row = mysqli_fetch_assoc($result)) {
                $link = "characterDetails.php?c_id=".$row["id"];
        ?>
        <tbody>
            <tr>
                <td><a href="<?php echo($link);?>"><?php echo $row["abreviation"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo($row["lastName"].", ".$row["firstName"]);?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["callsign"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["status"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["sname"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["hdate"];?></a></td>
            </tr>    
        </tbody>
        
        <?php } ?>
    </table>
</div>
<div class="pageSelect">
    <?php  createPageSelect($n_pages, $page, "characterList.php?".$sqnPost."&".$indexPost."&"); ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');