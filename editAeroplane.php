<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    // Accessing squadron data from database for editing.
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "s_id")) {
        
        $edit_id = filter_input(INPUT_GET, "s_id");
        $_SESSION["edit_id"] = $edit_id;
        $sql = "SELECT name, faction, type FROM aeroplanes WHERE id = $edit_id";
        $query = mysqli_query($dbx, $sql);
        $result = mysqli_fetch_assoc($query);
        $name = $result["name"];
        $faction = $result["faction"];
        $type = $result["type"];
    } 
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/aeroplaneAdminLogic.js"></script>
<script type="text/javascript">

window.onload = function(){
    gebid("changeAeroplane").addEventListener("click", changeAeroplane, false);
    gebid("deleteAeroplane").addEventListener("click", deleteAeroplane, false);
};   
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-510.1</p>
<h3>Edit Aeroplane ( ID: <?php echo $_SESSION["edit_id"]; ?> ): </h3>
<form id="editSquadron" onsubmit="return false;" >
    <div class='middlePageStandard'>
        <b>Aeroplane name:</b>
        <input type="text" id="aeroplaneName" name="aeroplaneName" value="<?php echo $name; ?>" size="50" maxlength="50">
        <p>The name of the aeroplane.</p>
    </div>
    
    <div class='middlePageStandard'>
            <b>Aeroplane faction:</b>
            <select id="aeroplaneFaction" name="aeroplaneFaction">
                <option value="RAF" <?php if($faction === "RAF"){ echo "selected";}?>>Royal Air Force</option>
                <option value="LW" <?php if($faction === "LW"){ echo "selected";}?>>Luftwaffe</option>
                <option value="VVS" <?php if($faction === "VVS"){ echo "selected";}?>>Voyenno-Vozdushnye Sily</option>
            </select>
            <p>The faction of the aeroplane.</p>
    </div>

    <div class='middlePageStandard'>
        <b>Aeroplane type:</b>
        <select id="aeroplaneType" name="aeroplaneType">
            <option value=1 <?php if($faction === 1){ echo "selected";}?>>Flyable aeroplane</option>
            <option value=2 <?php if($faction === 2){ echo "selected";}?>>Claimable aeroplane</option>
        </select>
        <p>The type of the aeroplane decides on which list it will be available.
        Flyable aeroplanes are selectable in the mission reports as own plane. 
        Claimable aeroplanes are selectable in the claims of mission reports.</p>
    </div>
    
    <div class='middlePageStandard'>
        <button id="changeAeroplane">Save Change</button><br>
        <span id="aChangeStatus" ></span>
    </div>
    
    <div class='middlePageStandard'>
        <p><button id="deleteAeroplane">Delete</button> Deletes aeroplane from 
        database. Only aeroplanes that have not been selected in reports or 
        claims should be deleted from the database for consistency.</p>
    </div>
</form>
<?php include(dirname(__FILE__).'/footer.php');
