<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    // Setting up indices to spread content over several pages.
    if(filter_has_var(INPUT_GET, "index")) {
        $indexLetter = filter_input(INPUT_GET, "index").'%';
    } else {
        $indexLetter = '%';
    }
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $n_entries = 18;
    $start_from = ($page - 1)*$n_entries;
    $dbx = getDBx();
    $sql = "SELECT acgmembers.id, acgmembers.username, acgmembers.callsign, ".
           "firststs2.date AS joiningdate, currentsts2.date AS lastStatus, acgmembers.admin, ".
           "memberstatus.status AS status, lwranks.name AS lwrname, rafranks.name AS rafrname ".
           "FROM acgmembers ".
           "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MIN(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS firststs ".
                "ON (firststs.memberID, firststs.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS firststs2 ".
            "ON firststs2.memberID = acgmembers.id ".
           "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MAX(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS currentsts ".
                "ON (currentsts.memberID, currentsts.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS currentsts2 ".
            "ON currentsts2.memberID = acgmembers.id ".
           "LEFT JOIN memberstatus ON currentsts2.statusID = memberstatus.id ".
           "LEFT JOIN ".
                "(SELECT promotions.memberid, promotions.value FROM promotions ".
                "JOIN ".
                    "(SELECT promotions.memberid, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                    "FROM promotions GROUP BY memberid) AS currentrank ".
                "ON (currentrank.memberid, currentrank.pdate) = ".
                "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
           "ON currentrank2.memberid = acgmembers.id ".
           "LEFT JOIN ranks AS lwranks ON (currentrank2.value, 'LW') = (lwranks.value, lwranks.faction) ".
           "LEFT JOIN ranks AS rafranks ON (currentrank2.value, 'RAF') = (rafranks.value, rafranks.faction) ".
           "WHERE callsign LIKE '$indexLetter' ORDER BY callsign ASC LIMIT $start_from, $n_entries";
    $result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(id) FROM acgmembers WHERE callsign LIKE '$indexLetter'";
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">
function addMember(){
    window.location = "addMember.php";
}    
function editMember(id){
    window.location = "editMember.php?m_id="+id;
}
function transferMember(id, name){
    window.location = "transferMember.php?m_id="+id+"&m_name="+name;
}
function characterMember(id, name){
    window.location = "memberCharacter.php?m_id="+id+"&m_name="+name;
} 
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-100.1</p>
<h3>Members Administration:</h3>
<div>
    <p>These pages list all registered TWC members with their corresponding names, 
    callsigns, joining- and eventually discharge dates, their status and possible 
    admin rights. New members can be added by filling out the "add Member" form.</p>
    <p>Existing members can be edited by filling out on of the "EDIT"-member forms. 
    This allows to edit the name, callsign, joining- or discharge date and admin rights.
    Members can be deleted from the roster.</p>

    
    <div>
        <button onclick="addMember()">Add Member</button>
    </div>
    <h3>ACG-Members:</h3>
</div>
<div class="indexSelect">
    <?php createPageIndex($indexLetter, 'memberAdministration.php?'); ?>
</div>
<div>
    <table class="wideTable">
        <thead>
            <tr>
                <th>ID:</th>
                <th>Callsign:</th>
                <th>Joining Date:</th>
                <th>Last status change:</th>
                <th>Current Status:</th>
                <th>Rights:</th>
                <th>Current rank:</th>
            </tr>
        </thead>
        <?php
            while($row = mysqli_fetch_assoc($result)) {               
        ?>
        <tbody>
            <tr>
                <td><?php echo $row["id"];?></td>
                <td><?php echo $row["callsign"];?></td>
                <td><?php echo $row["joiningdate"];?></td>
                <td><?php echo $row["lastStatus"];?></td>
                <td><?php echo $row["status"];?></td>
                <td><?php if($row["admin"]) {echo "Admin";}?></td>
                <td><?php echo($row["lwrname"]." / ".$row["rafrname"]);?></td>
                <td><button onclick="editMember(<?php echo $row['id']; ?>)">EDIT</button></td>
            </tr>    
        </tbody>
        
        <?php } ?>
    </table>
</div>
<div class="pageSelect">
    <?php  createPageSelect($n_pages, $page, 'memberAdministration.php?index='.$indexLetter.'&'); ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');