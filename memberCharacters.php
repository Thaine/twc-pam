<?php
    include(dirname(__FILE__).'/header0.php');
    
    $user_id = filter_input(INPUT_GET, "m_id");
    $dbx = getDBx();
    
    $sql = "SELECT callsign FROM acgmembers WHERE id = $user_id";
    $query = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($query);
    $callsign = $result["callsign"];
    
    //Get all character id's of member
    $sql = "SELECT careercharacters.id FROM careercharacters ".
           "WHERE careercharacters.personifiedBy = $user_id ORDER BY id";
    $cresult = mysqli_query($dbx, $sql);
    $c_id_array = mysqli_fetch_all($cresult);
    $character_array = array();
    for($n = 0; $n < count($c_id_array); $n++){
        
        $c_id = $c_id_array[$n][0];
        
        $sql = "SELECT careercharacters.firstname, careercharacters.lastname, ".
           "characterstatus.status AS cstatus, careercharacters.faction, ".
           "MAX(reports.dateSubmitted) AS mxrdate, MIN(reports.dateSubmitted) AS mnrdate ".
           "FROM careercharacters ". 
           "LEFT JOIN characterstatus ON careercharacters.characterstatus = characterstatus.id ".
           "LEFT JOIN reports ON careercharacters.id = reports.authorID ".
           "WHERE careercharacters.id = $c_id";
        // echo $sql;
        $mresult = mysqli_query($dbx, $sql);
        $mrow = mysqli_fetch_assoc($mresult);
        $character_array[$c_id]["firstName"] = $mrow["firstname"];
        $character_array[$c_id]["lastName"] = $mrow["lastname"];
        $character_array[$c_id]["status"] = $mrow["cstatus"];
        $faction = $mrow["faction"];
        $character_array[$c_id]["faction"] = $mrow["faction"];
        $mxrdate = $mrow["mxrdate"];
        $mnrdate = $mrow["mnrdate"];

        // Number of sorties
        $sql = "SELECT COUNT(*) FROM reports ".  
               "WHERE authorID=$c_id AND reports.accepted=1";
        $result = mysqli_query($dbx, $sql);
        $nreportsrow = mysqli_fetch_array($result);
        $character_array[$c_id]["sorties"] = $nreportsrow[0];
                
        if($faction == "RAF"){
            $sql = "SELECT claimstatusraf.status, SUM(1-claimsraf.shared*0.5) AS points ".
               "FROM claimsraf LEFT JOIN reports ON claimsraf.reportid = reports.id ".
               "LEFT JOIN claimstatusraf ON claimstatusraf.id = claimsraf.enemystatus ".
               "WHERE authorID = $c_id AND reports.accepted=1 AND claimsraf.accepted = 1 ".
               "GROUP BY claimsraf.enemystatus";
            $rafvictoriesresult = mysqli_query($dbx, $sql);
            $character_array[$c_id]["victories"] = $rafvictoriesresult;
        } else if($faction == "LW"){
            $sql = "SELECT COUNT(claimslw.id), claimslw.confirmed ".
               "FROM claimslw LEFT JOIN reports ON claimslw.reportid = reports.id ".
               "WHERE authorID = $c_id AND reports.accepted=1 AND claimslw.accepted = 1 ".
               "GROUP BY claimslw.confirmed";
            $lwvictoriesresult = mysqli_query($dbx, $sql);
            $character_array[$c_id]["victories"] = $lwvictoriesresult;
        }
        
        $sql = "SELECT SUM(claimsground.amount) ".
           "FROM claimsground LEFT JOIN reports ON claimsground.reportid = reports.id ".
           "WHERE authorID = $c_id AND reports.accepted=1 AND claimsground.accepted = 1";
        $groundvictoriesresult = mysqli_query($dbx, $sql);
        $character_array[$c_id]["gvictories"] = $groundvictoriesresult;
                
        // Get last rank
        $sql = "(SELECT ranks.abreviation, promotions.date, ranks.image, promotions.value ".
               "FROM promotions LEFT JOIN ranks ON (promotions.value, '$faction') = (ranks.value, ranks.faction) ".
               "RIGHT JOIN careercharacters ON promotions.memberID = careercharacters.personifiedBy ".
               "WHERE careercharacters.id = $c_id ".
               "AND promotions.date <= '$mxrdate' AND promotions.date > '$mnrdate' )".
               "UNION ".
               "(SELECT ranks.abreviation, promotions.date, ranks.image, promotions.value ".
               "FROM promotions LEFT JOIN ranks ON (promotions.value, '$faction') = (ranks.value, ranks.faction) ".
               "RIGHT JOIN careercharacters ON promotions.memberID = careercharacters.personifiedBy ".
               "WHERE careercharacters.id = $c_id ".
               "AND promotions.date <= '$mnrdate' ORDER BY date DESC LIMIT 1)". 
               "ORDER BY date DESC LIMIT 1";
                // echo $sql;
        $presult = mysqli_query($dbx, $sql);
        $promotiondates = mysqli_fetch_all($presult);
        if(count($promotiondates)>0){
            $character_array[$c_id]["rank"] = $promotiondates[0][0];
            $character_array[$c_id]["rankImage"] = $promotiondates[0][2];
            $character_array[$c_id]["rankValue"] = $promotiondates[0][3];
            $character_array[$c_id]["faction"] = $faction;
        }
        
             //Check if member has received his wings
        if($faction == "RAF"){
            $sql = "SELECT awards.abreviation, awards.image FROM awards ".
                   "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
                   "WHERE decorations.characterID = $c_id AND awards.abreviation = 'AB'";
            $wingsQuery = mysqli_query($dbx, $sql);
            if(mysqli_num_rows($wingsQuery)>0){
                $wingsrow = mysqli_fetch_assoc($wingsQuery);
                if($character_array[$c_id]["rankValue"]<8){
                    $character_array[$c_id]["wingsImage"] = $wingsrow["image"]."Fabric.png";
                } else {
                    $character_array[$c_id]["wingsImage"] = $wingsrow["image"]."Brass.png";
                }
            } else {
                $character_array[$c_id]["wingsImage"] = "";
            }
        } else if($faction == "LW"){
            $sql = "SELECT awards.abreviation, awards.image FROM awards ".
                   "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
                   "WHERE decorations.characterID = $c_id AND awards.abreviation = 'FBAgd'";
            $wingsQuery = mysqli_query($dbx, $sql);
            if(mysqli_num_rows($wingsQuery)>0){
                $wingsrow = mysqli_fetch_assoc($wingsQuery);
                $character_array[$c_id]["wingsImage"] = $wingsrow["image"];
            } else {
                $sql = "SELECT awards.abreviation, awards.image FROM awards ".
                   "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
                   "WHERE decorations.characterID = $c_id AND awards.abreviation = 'FBA'";
                $wingsQuery = mysqli_query($dbx, $sql);
                if(mysqli_num_rows($wingsQuery)>0){
                    $wingsrow = mysqli_fetch_assoc($wingsQuery);
                    $character_array[$c_id]["wingsImage"] = $wingsrow["image"];
                } else {
                    $character_array[$c_id]["wingsImage"] = "";
                }
            }
        }
    }
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/memberMenu.php'); ?> 
<p class="form_id">ACG-PAM/200-201.1</p>
<h3>Member profile:</h3>
<div>
    <p>This page shows the career characters for <?php echo $callsign; ?>. The displayed
    information is based on submitted and approved After Action Reports.</p>
    <hr>
    <?php 
    for($n = 0; $n < count($c_id_array); $n++){
        
        $c_id = $c_id_array[$n][0];
        $link = "characterDetails.php?c_id=".$c_id;
    ?>
    
    <a href="<?php echo($link);?>">
        <h3><?php echo($character_array[$c_id]["rank"]." ".
            $character_array[$c_id]["firstName"]." ".$character_array[$c_id]["lastName"]);?>:</h3>
    </a>
    <div class="uniform">
        <?php if($character_array[$c_id]["faction"] == "RAF"){ ?>
        <img src="imgsource/RAF-ranks/RAFUniform.png">
        <?php } else if($character_array[$c_id]["faction"] == "LW"){ ?>
        <img src="imgsource/LW-ranks/LWUniform.png">
        <?php } ?>
    </div>

    <div class="uniformRank">
        <?php if($character_array[$c_id]["faction"] == "RAF"){ ?>
        <img src="imgsource/RAF-ranks/<?php echo $character_array[$c_id]["rankImage"]; ?>">
        <?php } else if($character_array[$c_id]["faction"] == "LW"){ ?>
        <img src="imgsource/LW-ranks/<?php echo $character_array[$c_id]["rankImage"]; ?>">
        <?php } ?>
    </div>
    
    <div class="uniformWings">
        <?php if($character_array[$c_id]["wingsImage"] != ""){ ?>
        <img src="imgsource/medals-small/<?php echo $character_array[$c_id]["wingsImage"]; ?>">
        <?php } ?>
    </div >
    
    <div class="memberGeneralInformation">
        <table>
            <tr><td>Status:</td><td><?php echo $character_array[$c_id]["status"];?></td></tr>
        </table>
    </div>
    <div>
        <p>
            Sorties: <?php echo $character_array[$c_id]["sorties"];?>
            <?php
                if($character_array[$c_id]["faction"] == "RAF"){
                    while($row = mysqli_fetch_array($character_array[$c_id]["victories"])){
                        echo(" - ".$row[0].": ".$row[1]);
                    }
                } else if($character_array[$c_id]["faction"] == "LW"){
                    while($row = mysqli_fetch_array($character_array[$c_id]["victories"])){
                        $claimStatus = ($row[1])?"Confirmed: ":"Unconfirmed: ";
                        echo(" - ".$claimStatus.$row[0]);  
                    }  
                } 
                $row = mysqli_fetch_array($character_array[$c_id]["gvictories"]);
                if($row[0] > 0){
                    echo(" - Ground victories: ".$row[0]);
                }
            ?>
        </p>
        <img src="includes/awardStripesFactory.php?c_id_small=<?php echo $c_id; ?>">
    </div>
    <hr>
    <?php } ?>
    
</div>

<?php include(dirname(__FILE__).'/footer.php');