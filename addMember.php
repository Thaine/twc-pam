<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    $dbx = getDBx();

?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/memberAdminLogic.js"></script>
<script type="text/javascript">

window.onload = function(){
    gebid("membername").addEventListener("change", checkDBforMembername, false);
    gebid("callsign").addEventListener("change", checkDBforCallsign, false);
    gebid("addMemberButton").addEventListener("click", addMember, false);
};

</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?>
<p class="form_id">ACG-PAM/400-110.1</p>

<div>
    <h3>ACG enlistment:</h3>
    <p>Enlistment form for new TWC-members. All fields have to be filled in beforehand
    submission. Member name and callsign will be checked against the database for 
    availability. No duplicate member and/or callsigns are allowed. The member name
    has to be the same as used in the TWC-Forums since the login functions of the forum
    are used for login on the database. The callsign is used in the After Action Reports
    and should be the same as used ingame. Admin rights will grant the member rights
    to create, edit and delete members, comment on and accept 
    After Action Reports, as well as other administrative functions.</p>

    <hr>
    <form id="addNewMemberForm" onsubmit="return false;" >
    <div class="middlePageStandard">
        <b>Member name:</b>
        <input type="text" id="membername" name="membername">
        <span id="mnamestatus" ></span>
        <p>This has to be the same name as used in the TWC-Forums.
        Allowed symbols are letters, numbers and whitespaces.</p>
    </div>
   
    <div class="middlePageStandard">
        <b>Member callsign:</b>
        <input type="text" id="callsign" name="callsign" >
        <span id="callsignstatus" ></span>
        <p>Shorter name for usage in After Action Reports. It should be
        the same callsign as used ingame. Allowed symbols are letters
        and numbers.</p>
    </div>
        
    <div class="middlePageStandard">
        <b>Joining date (YYYY-MM-DD):</b>
        <input type="text" id="jdateY" name="jdateY" size="4" maxlength="4" >-
        <input type="text" id="jdateM" name="jdateM" size="2" maxlength="2" >-
        <input type="text" id="jdateD" name="jdateD" size="2" maxlength="2" >
    </div>
    
    <div class="middlePageStandard">
        <b>Admin rights:</b>
        <input id="admin" name="admin" type="checkbox" value="on" >
        <p>This will grant the member rights to add/edit/edit members, missions,
        etc.</p>
    </div>
    
    <div id="submitStatus"  class="middlePageStandard">&nbsp;</div>
    <button type=button id="addMemberButton">Add Member</button>
</form>

</div>
<?php include(dirname(__FILE__).'/footer.php'); ?>