
function randomFName(faction){
    var ajax1 = syncAjaxObj("POST", "./includes/characterLogic.php");
    ajax1.send("randmFName="+faction);
    return ajax1.responseText;
}

function randomLName(faction){
    var ajax2 = syncAjaxObj("POST", "./includes/characterLogic.php");
    ajax2.send("randmLName="+faction);
    return ajax2.responseText;
}

function createCharacter(faction){
    var f = gebid("firstName").value;
    var l = gebid("lastName").value;
    if (f === "") {
        f = randomFName(faction);
    }
    if (l === "") {
        l = randomLName(faction);
    }
    var rx = /[^a-z]/gi;
    if(rx.test(f) || rx.test(l)){
        gebid("characterStatus").innerHTML = 'Name has wrong format.';
    } else if (f === "" || l === "") {
        gebid("characterStatus").innerHTML = 'Name must not be empty.';
    } else {
        gebid("characterStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/characterLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                 gebid("characterStatus").innerHTML = ajax.responseText;
                if(ajax.responseText === "Character created."){
                    window.location.reload();
                }
            }
        };
        ajax.send("addCharacter="+f+"&lname="+l+"&f="+faction);
    }
}

function editCharacterStatus(id, divID){
    var status = gebid(divID).value;
    gebid("characterStatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/characterLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            
            if(ajax.responseText === "Character status edited.") {
                window.location.reload();
            }
        }
    };
    ajax.send("editCharacterStatus="+id+"&s="+status);
}

function editCharacterName(id){
    
    var f = gebid("firstName").value;
    var l = gebid("lastName").value;
    postStr = "editCharacter=" + id;
    if(f !== ""){
        postStr = postStr + "&f=" + f;
    }
    if(l !== "") {
        postStr = postStr + "&l=" + l;
    } 
    if(f !== "" || l !== "") {
        gebid("characterStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/characterLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("characterStatus").innerHTML = ajax.responseText;
                if(ajax.responseText === "Character name edited."){
                    window.location.reload();
                }
            }
        };
        ajax.send(postStr);
    }
}

function deleteCharacter(id){
    if(confirm("Do you realy want to delete this character?")) {
        var ajax = ajaxObj("POST", "./includes/characterLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                window.location.reload();
            }
        };
        ajax.send("deleteCharacter="+id);  
    }
}

function newCharacterQueryAnswer(wantsNewCharacter, memberID, characterID, mission_id, report, squadron, faction){

    var ajax = ajaxObj("POST", "./includes/characterLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            
            if(ajax.responseText === "Character created.") {
                if(report){
                    editReport(mission_id, memberID);
                } else {
                    reportForDuty(mission_id, squadron, memberID, faction);
                }
                
            } else {
                alert(ajax.responseText);
            }
        }
    };
    ajax.send("newCharacterQuery="+wantsNewCharacter+"&m_id="+memberID+"&c_id="+characterID);
}
