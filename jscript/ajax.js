// This function is based on the developephp Ajax Framework Tutorial.
// http://www.developphp.com/view.php?tid=1288

function ajaxObj( meth, url ) {
    var x;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        x = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        x = new ActiveXObject("Microsoft.XMLHTTP");
    }
    x.open( meth, url, true );
    x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    return x;
}

function ajaxReturn(x){
    if(x.readyState === 4 && x.status === 200){
        return true;	
    }
}

function syncAjaxObj( meth, url ) {
    var x;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        x = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        x = new ActiveXObject("Microsoft.XMLHTTP");
    }
    x.open( meth, url, false );
    x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    return x;
}
