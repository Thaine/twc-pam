function addAeroplane(){
    var n = gebid("aeroplaneName").value;
    var f = gebid("aeroplaneFaction").value;
    var t = gebid("aeroplaneType").value;
    var rx = /[^a-z0-9/.\- ]/gi;
    if(rx.test(n)){
        gebid("addAeroplaneStatus").innerHTML = 'Name has wrong format.';
    } else if (n === "") {
        gebid("addAeroplaneStatus").innerHTML = 'Name must not be empty.';
    } else {
        gebid("addAeroplaneStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/aeroplaneAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("addAeroplaneStatus").innerHTML = ajax.responseText;
                if(ajax.responseText === "Aeroplane added."){
                    window.location.reload();
                }
            }
        };
        ajax.send("addAeroplane="+n+"&f="+f+"&t="+t);
    }
}

function changeAeroplane(){
    var n = gebid("aeroplaneName").value;
    var f = gebid("aeroplaneFaction").value;
    var t = gebid("aeroplaneType").value;
    var rx = /[^a-z0-9/.\- ]/gi;
    if(rx.test(n)){
        gebid("aChangeStatus").innerHTML = 'Name has wrong format.';
    } else if (n === "") {
        gebid("aChangeStatus").innerHTML = 'Name must not be empty.';
    } else {
        gebid("aChangeStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/aeroplaneAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("aChangeStatus").innerHTML = ajax.responseText;
                if(ajax.responseText === "Changes saved."){
                    window.location.reload();
                }
            }
        };
        ajax.send("editAeroplane="+n+"&f="+f+"&t="+t);
    }
}

function deleteAeroplane(){
    if(confirm("Do you realy want to delete this aeroplane?")) {
        var ajax = ajaxObj("POST", "./includes/aeroplaneAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                window.location.href="./aeroplaneAdministration.php";
            }
        };
        ajax.send("deleteAeroplane=true");  
    }
}
    





