function promoteMember(){
    var s = gebid("rank").value;
    var y = gebid("pdateY").value;
    var m = gebid("pdateM").value;
    var d = gebid("pdateD").value;
    var c = encodeURIComponent(gebid("pcomment").value);
    var rx = /[^0-9]/gi;
    if(rx.test(y) || rx.test(m) || rx.test(d)){
        gebid("promoteStatus").innerHTML = 'Promotion/Demotion date has wrong format.';
    } else {
        gebid("promoteStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/promoteMemberLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("promoteStatus").innerHTML = ajax.responseText;
                window.location.reload();
            }
        };
        ajax.send("promote="+s+"&y="+y+"&m="+m+"&d="+d+"&c="+c); 
    }
}

function deletePromotion(id){
    if(confirm("Do you realy want to delete this promotion/demotion?")) {
        var ajax = ajaxObj("POST", "./includes/promoteMemberLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("promoteStatus").innerHTML = ajax.responseText;
                window.location.reload();
            }
        };
        ajax.send("deletePromotion="+id);  
    }
}