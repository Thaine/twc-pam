<?php
    include(dirname(__FILE__).'/header0.php');
    
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    $dbx = getDBx();
    
    if(filter_has_var(INPUT_GET, "r_id")){
        $report_id = filter_input(INPUT_GET, "r_id");
    } else {
        header("location: missionList.php");
        exit();
    }
       
    $user_id = $_SESSION["userID"];
    //Load this report
    
    $sql = "SELECT flightsraf.name AS flight, sectionraf.name AS section, sectionpos.name AS sectionpos, ".
           "careercharacters.firstname, careercharacters.lastname, acgmembers.callsign, ".
           "acgmembers.id AS m_id, ".
           "reports.aerodrome, reports.markings, reports.synopsis, ".
           "aeroplanes.name AS aeroplane, ".
           "reportdetailsraf.serialno ,".
           "pilotstatus.status AS pilotstatus, aeroplanestatus.status AS aeroplanestatus, ".
           "reports.accepted, reports.dateSubmitted ".
           "FROM reports ".
           "LEFT JOIN reportdetailsraf ON reportdetailsraf.reportid = reports.id ".
           "LEFT JOIN flightsraf ON flightsraf.id = reportdetailsraf.flight ".
           "LEFT JOIN sectionraf ON sectionraf.id = reportdetailsraf.section ". 
           "LEFT JOIN sectionpos ON sectionpos.id = reportdetailsraf.sectionpos ".
           "LEFT JOIN careercharacters ON reports.authorid = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
           "LEFT JOIN aeroplanes ON aeroplanes.id = reports.aeroplane ".
           "LEFT JOIN pilotstatus ON pilotstatus.id = reports.pilotstatus ".
           "LEFT JOIN aeroplanestatus ON aeroplanestatus.id = reports.aeroplanestatus ".
           "WHERE reports.id = $report_id";
    
    $query = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($query)>0) {
        
        $r_result = mysqli_fetch_assoc($query);
        
        $flight = $r_result["flight"];
        $section = $r_result["section"];
        $sectionPos = $r_result["sectionpos"];
        
        $firstName = $r_result["firstname"];
        $lastName = $r_result["lastname"];
        $callsign = $r_result["callsign"];
        $m_id = $r_result["m_id"];
        $m_link = "memberDetails.php?m_id=".$m_id;
        
        $aerodrome = $r_result["aerodrome"];
        $aeroplane = $r_result["aeroplane"];
        $markings = $r_result["markings"];
        $serialNo = $r_result["serialno"];
        $synopsis = parseImageTags($r_result["synopsis"]);
        
        $dateSubmitted = $r_result["dateSubmitted"];
        
        $pilotStatus = $r_result["pilotstatus"];
        $aeroplaneStatus = $r_result["aeroplanestatus"];
        
        $accepted = $r_result["accepted"];
        
    } else {
       
        //TODO: REDIRECT SCRIPT!
//        header("location: missionList.php");
//        exit();
    }
       
    // Load claims from database and add them to the form
    $sql = "SELECT aeroplanes.name, claimsraf.id, claimstatusraf.status, claimsraf.shared, ".
           "claimsraf.description, claimsraf.accepted ". 
           "FROM claimsraf ".
           "RIGHT JOIN reports ON reports.id = claimsraf.reportid ".
           "LEFT JOIN aeroplanes ON aeroplanes.id = claimsraf.aeroplane ".
           "LEFT JOIN claimstatusraf ON claimstatusraf.id = claimsraf.enemystatus ".
           "WHERE reports.id = $report_id";
    $claim_result = mysqli_query($dbx, $sql);

    // Load ground claims from database and add them to the form
    $sql = "SELECT groundtargets.name, claimsground.id, claimsground.amount, ".
           "claimsground.description, claimsground.accepted ". 
           "FROM claimsground ".
           "RIGHT JOIN reports ON reports.id = claimsground.reportid ".
           "LEFT JOIN groundtargets ON groundtargets.id = claimsground.object ".
           "WHERE reports.id = $report_id";
//    echo $sql;
    $groundclaim_result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT ranks.abreviation ".
           "FROM ranks ".
           "RIGHT JOIN promotions ON (ranks.faction, ranks.value) = ('RAF', promotions.value) ".
           "JOIN ".
                "(SELECT promotions.memberid, promotions.date, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                "FROM promotions WHERE date <= '$mi_realDate' GROUP BY memberid) AS lastPromotionDates ".
           "ON (lastPromotionDates.memberid, lastPromotionDates.pdate) = 
                (promotions.memberid, UNIX_TIMESTAMP(promotions.date)) ".
           "WHERE promotions.memberid = $m_id";
    $rank_querry = mysqli_query($dbx, $sql);
    $rank_result = mysqli_fetch_assoc($rank_querry);
    $rank = $rank_result["abreviation"];
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<script src="jscript/reportAdminLogic.js"></script>
 
<?php include(dirname(__FILE__).'/reportMenu.php'); ?> 
<p class="form_id">ACG-PAM/300-540.1</p>
<h3>After Action Report:</h3>

<div>
    <span class="AARSpanFlightLeft">Flight/Section/Pos:</span>
    <span class="AARSpanFlightRight">
        <?php echo($flight." / ".$section." / ".$sectionPos); ?>
    </span>
</div> 

<div>
    <span class="AARSpanLeft">Name:</span>
    <span class="AARSpanRight"><a href="<?php echo($m_link);?>"><?php echo($rank." ".$firstName." '".$callsign."' ".$lastName); ?></a></span>
</div>

<div>
    <span class="AARSpanLeft">Date:</span>
    <span class="AARSpanRight"><?php echo($dateSubmitted); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Base:</span>
    <span class="AARSpanRight"><?php echo($aerodrome); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Type:</span>
    <span class="AARSpanRight"><?php echo($aeroplane); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Markings:</span>
    <span class="AARSpanRight"><?php echo($markings); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Serial Nr.:</span>
    <span class="AARSpanRight"><?php echo($serialNo); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Synopsis:</span><br>
    <p id="synopsis"><?php echo(nl2br($synopsis)) ?></p>
</div>

<div id="claims">
    <hr>
    Claims:<br>
    <table>
<?php
    while($r_row = mysqli_fetch_assoc($claim_result)) {
        
        $claim_id = $r_row["id"];
        $claim_accepted = $r_row["accepted"];
        if(!is_null($claim_id)){
?>
    <tr >
        <td><?php echo($r_row["name"]);?></td>
        <td><?php echo($r_row["status"]);?></td>
        <td><?php echo($r_row["shared"]? "Shared" : "");?></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="4"><?php echo($r_row["description"]);?></td>
    </tr>
<?php
        }
    }
?>
    </table>
</div>
<div id="claims">Ground claims:<br>
    <hr>
    <table>
    <?php 
        while($r_row = mysqli_fetch_assoc($groundclaim_result)) {
        
        $claim_id = $r_row["id"];
        $claim_accepted = $r_row["accepted"];
        if(!is_null($claim_id)){
    ?>
    <tr>
        <td><?php echo($r_row["name"]);?></td>
        <td><?php echo("Amount: ".$r_row["amount"]);?></td>
        
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="3"><?php echo($r_row["description"]);?></td>
    </tr>
<?php
        }
    }
?>
    </table>
</div>
<div>
    <hr>
    <span class="AARSpanLeft">Pilot status:</span>
    <span class="AARSpanRight"><?php echo($pilotStatus); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Aircraft status:</span>
    <span class="AARSpanRight"><?php echo($aeroplaneStatus); ?></span>
    <hr>
</div>


<?php include(dirname(__FILE__).'/footer.php');