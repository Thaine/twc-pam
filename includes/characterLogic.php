<?php
include_once(dirname(__FILE__).'/db_connect.php');
include_once(dirname(__FILE__).'/characterDBFunctions.php');
include_once(dirname(__FILE__).'/functions.php');
session_start();
$dbx = getDBx();

if(filter_has_var(INPUT_POST, "randmFName")){

    $faction = filter_input(INPUT_POST, "randmFName");
    echo randName($faction, "FirstName", $dbx);
    exit();  
}

if(filter_has_var(INPUT_POST, "randmLName")){
    
    $faction = filter_input(INPUT_POST, "randmLName");
    echo randName($faction, "LastName", $dbx);
    exit();  
}

if(filter_has_var(INPUT_POST, "addCharacter")) {
    $edit_id = $_SESSION["edit_id"];
    $f = filter_input(INPUT_POST, "addCharacter", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $l = filter_input(INPUT_POST, "lname", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $faction = filter_input(INPUT_POST, "f", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $sql = "SELECT id, characterstatus FROM careercharacters ".
           "WHERE personifiedby = $edit_id AND characterstatus = 1 ".
           "AND faction = '$faction'";
    $query = mysqli_query($dbx, $sql);
    $character_check = mysqli_num_rows($query);
    if($character_check > 0){
        echo "Member has already an active character for the $faction.";
        exit();
    }
    if(addCharacter($edit_id, $f, $l, $faction, $dbx)){
        echo "Character created.";
        exit();
    } else {
        echo "Error during database manipulation.";
        exit();
    }   
}

if(filter_has_var(INPUT_POST, "deleteCharacter")){
    $id = filter_input(INPUT_POST, "deleteCharacter");
    $sql = "DELETE FROM careercharacters WHERE id='$id'";
    if(mysqli_query($dbx, $sql)) {
        echo "deleted";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "editCharacter")){
    $id = filter_input(INPUT_POST, "editCharacter");
    $sql = "UPDATE careercharacters SET ";
    if(filter_has_var(INPUT_POST, "f")){
       $f = filter_input(INPUT_POST, "f");
       $sql = $sql."firstname = '$f' ";
    }
    if(filter_has_var(INPUT_POST, "l")){
       if(filter_has_var(INPUT_POST, "f")){
           $sql = $sql.", ";
       } 
       $l = filter_input(INPUT_POST, "l");
       $sql = $sql."lastname = '$l' ";
    }
    $sql = $sql."WHERE id = $id";
    if(mysqli_query($dbx, $sql)){
        echo "Character name edited.";
        exit();
    } else {
        echo "Error during database manipulation (character name edit).";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "editCharacterStatus")){
    $id = filter_input(INPUT_POST, "editCharacterStatus");
    $status = filter_input(INPUT_POST, "s");
    $sql = "UPDATE careercharacters SET characterstatus=$status ".
           "WHERE id = $id";
    if(mysqli_query($dbx, $sql)){
        echo "Character status edited.";
        exit();
    } else {
        echo "Error during database manipulation (status edit)";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "newCharacterQuery")){
    
    $wantsNewCharacter = filter_input(INPUT_POST, "newCharacterQuery");
    $memberID = filter_input(INPUT_POST, "m_id");
    $c_id = filter_input(INPUT_POST, "c_id");

    if($wantsNewCharacter){
        
        //Dismiss old character
        $sql = "UPDATE careercharacters SET characterstatus=4 ".
           "WHERE id = $c_id";
        if(!mysqli_query($dbx, $sql)){
            echo "Error during database manipulation (status edit)";
            exit();
        }
        $faction = getFaction($memberID, $dbx);
        $c_id = createCharacter($memberID, $faction, $dbx);
        if(!$c_id){

            echo "Error during database manipulation (character creation).";
            exit();
        }
    }
        
    $_SESSION["designatedCharacter"] = $c_id;
    echo "Character created.";
    exit();
}