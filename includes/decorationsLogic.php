<?php
include_once(dirname(__FILE__).'/db_connect.php');
include_once(dirname(__FILE__).'/functions.php');
$dbx = getDBx();

if(filter_has_var(INPUT_POST, "awardDecoration")){
    $id = filter_input(INPUT_POST, "awardDecoration");
    $y = filter_input(INPUT_POST, "y");
    $m = filter_input(INPUT_POST, "m");
    $d = filter_input(INPUT_POST, "d");
    $c = filter_input(INPUT_POST, "c", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $ab = filter_input(INPUT_POST, "ab");

    if($y==="" | $m==="" | $d===""){
        $ddate = date("Y-m-d");
    } else {
        $check_dDate = mktime(0, 0, 0, $m, $d, $y);
        $ddate = date("Y-m-d", $check_dDate);
    }
    
    $sql = "SELECT decorations.availableSince FROM decorations WHERE id = $id";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $availableSince = $row["availableSince"];
    
    if(strtotime($ddate) < strtotime($availableSince)){
        $ddate = $availableSince;
    }
    
    $sql = "UPDATE decorations SET awarded=true, date='$ddate', comment='$c', awardedBy=$ab ".
           "WHERE id = $id";

    if(mysqli_query($dbx, $sql)){
        echo "decoration awarded.";
        exit();
    } else {
        echo "Error during database manipulation (decoration award). $sql";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "revokeDecoration")){
    $id = filter_input(INPUT_POST, "revokeDecoration");
    
    $sql = "UPDATE decorations SET awarded=false, awardedBy='null'".
           "WHERE id = $id";
    if(mysqli_query($dbx, $sql)){
        echo "decoration revoked.";
        exit();
    } else {
        echo "Error during database manipulation (decoration revoke).";
        exit();
    }
    
}