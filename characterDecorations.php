<?php 
    include(dirname(__FILE__).'/header0.php'); 
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');

    $member_id = filter_input(INPUT_GET, "c_id");
    $dbx = getDBx();
    
    $sql = "SELECT awards.name, awards.image, decorations.date, decorations.comment, ".
           "acgmembers.callsign FROM decorations ".
           "LEFT JOIN awards ON decorations.awardID = awards.id ".
           "LEFT JOIN acgmembers ON decorations.awardedBy = acgmembers.id ".
           "WHERE decorations.characterID = $member_id AND decorations.awarded = 1 ".
           "ORDER BY date ASC" ;
//    echo $sql;
    $dquerry = mysqli_query($dbx, $sql);
    
    $sql = "SELECT careercharacters.firstName, careercharacters.lastName, ".
           "acgmembers.callsign FROM careercharacters ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "WHERE careercharacters.id = $member_id";
    $cresult = mysqli_query($dbx, $sql);
    $crow = mysqli_fetch_row($cresult);
    $firstName = $crow[0];
    $lastName = $crow[1];
    $callsign = $crow[2];
    
    $sql = "SELECT missions.realDate, missions.histDate FROM missions";
    $miresult = mysqli_query($dbx, $sql);
    $missiondates = mysqli_fetch_all($miresult);
    
    
?>    
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/memberMenu.php'); ?> 
<p class="form_id">ACG-PAM/200-211.1</p>
<h3>Character profile:</h3>
<div>
    <p>This page shows a list of received decorations by 
        <?php echo($firstName." '".$callsign."' ".$lastName);?>.</p>
    
    <h3>Received decorations:</h3>
    <table style="border-collapse:collapse;">
        <thead>
            <tr>
                <th></th>
                <th align="left">Date:</th>
                <th align="left">Name:</th>
                <th align="left">Awarded by:</th>
            </tr>
        </thead>
        <?php
            while($row = mysqli_fetch_assoc($dquerry)){
                
                //Special check for RAF Wings.
                if($row["name"]=="RAF Aircrew Brevet"){
                    $sql = "SELECT MAX(reports.missionID) AS mxid FROM reports WHERE reports.authorID = $member_id";
                    $querry = mysqli_query($dbx, $sql);
                    $result = mysqli_fetch_assoc($querry);
                    $missionID = $result["mxid"];

                    $sql = "SELECT careercharacters.personifiedBy FROM careercharacters ".
                           "WHERE careercharacters.id = $member_id";
                    $querry = mysqli_query($dbx, $sql);
                    $result = mysqli_fetch_assoc($querry);
                    $memberID = $result["personifiedBy"];
                    $rankValue = getRankValueAtMission($memberID, $missionID, $dbx);
                    
                    if($rankValue < 8){
                        $row["image"] = $row["image"]."Fabric.png";
                    } else {
                        $row["image"] = $row["image"]."Brass.png";
                    }
                }
        ?>
        <tbody>
            <tr style="border-top: black 1px solid;">
                <td width="10%" rowspan="2"><img src="imgsource/medals-big/<?php echo $row["image"]; ?>"></td>
                <td width="10%"><?php echo realToHistDate($row["date"], $missiondates); ?></td>
                <td width="50%"><?php echo $row["name"]; ?></td>
                <td width="10%"><?php echo $row["callsign"]; ?></td>
            </tr>
            <tr>
                <td colspan="3"><?php echo $row["comment"]; ?>&nbsp;</td>
            </tr>
        </tbody>
        
        <?php } ?>
    </table>
</div>
<?php include(dirname(__FILE__).'/footer.php');
