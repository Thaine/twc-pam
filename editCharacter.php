<?php
    include(dirname(__FILE__).'/header0.php');
    include(dirname(__FILE__).'/includes/characterDBFunctions.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    //Accessing data for member from database for editing.
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "m_id")) {
        
        $edit_id = filter_input(INPUT_GET, "m_id");
        $_SESSION["edit_id"] = $edit_id;
        $sql = "SELECT callsign FROM acgmembers WHERE id = $edit_id";
        $query = mysqli_query($dbx, $sql);
        $result = mysqli_fetch_assoc($query);
        $callsign = $result["callsign"];
        
        // Access character data from database for editing.
        $sql = "SELECT careercharacters.id, careercharacters.firstname, careercharacters.lastname, ".
               "characterstatus.status, careercharacters.faction, careercharacters.characterstatus ".
               "FROM careercharacters ".
               "LEFT JOIN characterstatus ON characterstatus.id = careercharacters.characterstatus ". 
               "WHERE personifiedby = $edit_id";
        $c_query = mysqli_query($dbx, $sql);
        $c_array = mysqli_fetch_all($c_query, MYSQLI_ASSOC);
        
        $sql = "SELECT id, status FROM characterstatus";
        $cs_query = mysqli_query($dbx, $sql);
        $cs_array = mysqli_fetch_all($cs_query, MYSQLI_NUM);
                
    } 
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/characterLogic.js"></script>
<script type="text/javascript">
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-120.1</p>

<h3>Administrate characters for <?php echo $callsign;?> ( ID: <?php echo $_SESSION["edit_id"]; ?> ): </h3>
<hr>

<form id="newCharacterForm" onsubmit="return false;" >
    <div class="middlePageStandard">
    <p>Create, delete or dismiss career characters. Career characters represent 
    pilots that are bound to their fate. While members keep on flying sorties 
    and collecting victories, no matter the outcome of each mission, career characters
    can only fly sorties as long as they are not killed or captured. Once a 
    career character is killed or captured, a new character is created. It is 
    possible to dismiss characters in case members leave TWC or for other reasons.</p>
    <p>The first and/or last name of a career characters can be edited by typing
    the new name into the filed for first and/or last name, followed by pressing
    the <i>Change Name</i> button next to the character concerning the name change.</p>
    </div>
    <div class="middlePageStandard">
        <b>First name:</b>
        <input type="text" id="firstName" name="firstName">
        <span id="fnamestatus" ></span>
    </div>

    <div class="middlePageStandard">
        <b>Last name:</b>
        <input type="text" id="lastName" name="lastName">
        <span id="lnamestatus" ></span>
    </div>
    <div id="characterStatus">
        <button onclick="createCharacter('RAF')">Create RAF character</button>
        <button onclick="createCharacter('LW')">Create LW character</button>
        <span id="characterStatus">&nbsp;</span> 
    </div>     
</form>

<div class="middlePageStandard">
    <form id="editCharacterForm" onsubmit="return false;">
    <h3>Characters:</h3>
    <hr>
    <table>
        <tr>
            <th>ID:</th>
            <th>First name:</th>
            <th>Last name:</th>
            <th>Faction:</th>
            <th>Status:</th>  
        </tr> 
    <?php 
        for($n = 0; $n < count($c_array); $n++){
                $row = $c_array[$n];
                $id = $row["id"];
    ?>   
        <tr>
            <td><?php echo $id;?></td>
            <td><?php echo $row["firstname"];?></td>
            <td><?php echo $row["lastname"];?></td>
            <td><?php echo $row["faction"]; ?></td>
            <td><?php echo $row["status"];?></td>
            <td><?php createSelectForm("cs".$id, $cs_array, $row["characterstatus"]);?></td>
            <td><button onclick="editCharacterStatus(<?php echo $id; ?>, '<?php echo "cs".$id; ?>')">Change status</button></td>
            <td><button onclick="editCharacterName(<?php echo $row['id']; ?>)">Change name</button></td>
            <td><button onclick="deleteCharacter(<?php echo $row['id']; ?>)">Delete</button></td>
        </tr>
    <?php } ?>
    </table>
    </form>
</div>
            
<?php include(dirname(__FILE__).'/footer.php');