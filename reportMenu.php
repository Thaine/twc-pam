</head>
<body id="adminMenuBody">
    <div class="mainContainer">
        <div class="pageTop">
            <?php
            include(dirname(__FILE__).'/primMenu.php'); 
            include(dirname(__FILE__).'/userMenu.php');
            ?>
            <div class="secMenu">
                <ul>
                    <li><a href="reportList.php">All</a></li>
                    <li><a href="reportList.php?fac=1">RAF</a></li>
                    <li><a href="reportList.php?fac=2">LW</a></li>
                </ul>
            <?php if($_SESSION["username"] !== "Visitor") { ?>
                <ul>
                    <li></li>
                    <li><a href="createReportRAF.php">Write RAF report</a></li>
                    <li><a href="createReportLW.php">Write LW report</a></li>
                    <?php  
                        
                        $isReportPage = false;
                        $link = "";
                        if($_SERVER['PHP_SELF'] == "/reportRAF.php"){
                            $isReportPage = true;
                            $link = "createReportRAF.php?r_id=$report_id";
                        }
                        if($_SERVER['PHP_SELF'] == "/reportLW.php"){
                            $isReportPage = true;
                            $link = "createReportLW.php?r_id=$report_id";
                        }
                        $isAuthor = ($user_id == $m_id);
                        if($isAuthor & $isReportPage){

                    ?>
                    <li><a <?php echo "href=$link"; ?>>Edit report</a></li>
                    <?php } ?>
                </ul>
            <?php } ?>
            </div>
        </div>
        <div class="pageMiddle">