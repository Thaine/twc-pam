<?php
    include(dirname(__FILE__).'/header0.php');
    
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    
    $dbx = getDBx();

    // Check if existing user otherwise redirect
    if(!isset($_SESSION["userID"])){
        header("location: message.php?m=1");
        exit();
    }
    
    $user_id = $_SESSION["userID"];
    $sql = "SELECT date, statusID FROM memberstatuslog WHERE memberID = $user_id ".
           "ORDER BY date DESC LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    $u_result = mysqli_fetch_assoc($query);
       
    // Check if user is still an active member otherwise redirect
    if($u_result["status"] === "2"){

        header("location: message.php?m=1");
        exit();
    } else {
        $sql = "SELECT callsign FROM acgmembers WHERE id = $user_id ";
        $query = mysqli_query($dbx, $sql);
        $u_result = mysqli_fetch_assoc($query);
    }
    $callsign = $u_result["callsign"];
    $faction = "LW";
    
    if(filter_has_var(INPUT_GET, "r_id")){
        $r_id = filter_input(INPUT_GET, "r_id");
    } else {
        $r_id = -1;
    }
       
    // Check if user has already submitted a report. If yes: load this report for editing
    $sql = "SELECT reports.id AS r_id, reports.aerodrome, reports.aeroplane, reports.markings, ".
           "reports.synopsis, reports.aeroplanestatus, reports.pilotstatus, careercharacters.id AS c_id,".
           "careercharacters.firstname, careercharacters.lastname, ".
           "reportdetailslw.id AS rd_id, reportdetailslw.swarm, reportdetailslw.swarmpos ".
           "FROM reports LEFT JOIN careercharacters ON reports.authorid = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
           "LEFT JOIN reportdetailslw ON reports.id = reportdetailslw.reportid ".
           "WHERE reports.id = $r_id AND acgmembers.id = $user_id";
//    echo($sql);
    $query = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($query)>0) {
        
        $r_result = mysqli_fetch_assoc($query);
        $firstName = $r_result["firstname"];
        $lastName = $r_result["lastname"];
        $c_id = $r_result["c_id"];
        
        $aerodrome = $r_result["aerodrome"];
        $aeroplane = $r_result["aeroplane"];
        $swarm = $r_result["swarm"];
        $swarmPos = $r_result["swarmpos"];
        $markings = $r_result["markings"];
        $synopsis = $r_result["synopsis"];
        $pilotStatus = $r_result["pilotstatus"];
        $aeroplaneStatus = $r_result["aeroplanestatus"];
        
    } else {
        
        $r_id = -1;
        $aerodrome = NULL;
        $aeroplane = NULL;
        $swarm = NULL;
        $swarmPos = NULL;
        $markings = NULL;
        $synopsis = NULL;
        $pilotStatus = NULL;
        $aeroplaneStatus = NULL;
        
        // Check if a character is available
        $sql = "SELECT id FROM careercharacters ".
               "WHERE characterstatus = 1 AND personifiedBy = $user_id AND faction = 'LW'".
               "ORDER BY id ASC LIMIT 1";
        $query = mysqli_query($dbx, $sql);
        $character_check = mysqli_num_rows($query);
        if($character_check < 1) {

            include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
            // $c_id = createCharacter($user_id, $faction, $dbx);
            $firstName = randName($faction, "FirstName", $dbx);
            $lastName = randName($faction, "LastName", $dbx);
            $c_id = -1;

        } else {

            $c_result = mysqli_fetch_assoc($query);
            $c_id = $c_result["id"];

        }  
    }
    
    // Access al swarm info from database for form input.
    $sql = "SELECT id, name FROM swarmslw";
    $swarms_result = mysqli_query($dbx, $sql);
    
    // Access al sectionPos info from database for form input.
    $sql = "SELECT id, name FROM swarmpos";
    $swarmPos_result = mysqli_query($dbx, $sql);
    
    // Access al pilotStatus info from database for form input.
    $sql = "SELECT id, status FROM pilotstatus";
    $pilotStatus_result = mysqli_query($dbx, $sql);
    
    // Access al aeroplaneStatus info from database for form input.
    $sql = "SELECT id, status FROM aeroplanestatus";
    $aeroplaneStatus_result = mysqli_query($dbx, $sql);
        
    // Load flyable (type=1) aeroplanes from the database for form input
    $sql = "SELECT id, name FROM aeroplanes ".
           "WHERE faction = '$faction' AND type = 1";
    $aeroplane_flyable_result = mysqli_query($dbx, $sql);
    
    // Load claimable (type=2) aeroplanes from the database for form input
    $sql = "SELECT id, name FROM aeroplanes ".
           "WHERE faction != '$faction' AND type = 2";
    $aeroplane_claimable_result = mysqli_query($dbx, $sql);
    $aeroplane_claimable_array = mysqli_fetch_all($aeroplane_claimable_result);
    
    // Load ground targets from the database for form input
    $sql = "SELECT id, name FROM groundtargets";
    $groundTarget_result = mysqli_query($dbx, $sql);
    $groundTarget_array = mysqli_fetch_all($groundTarget_result);
    
    // Access al typeOfDestr of the LW from database for form input
    $sql = "SELECT id, type FROM typeofdestr";
    $typeOfDestr_result = mysqli_query($dbx, $sql);
    $typeOfDestr_array = mysqli_fetch_all($typeOfDestr_result);
    unset($typeOfDestr_array[6]);
    
    // Access al typeOfImpact of the LW from database for form input
    $sql = "SELECT id, type FROM typeofimpact";
    $typeOfImpact_result = mysqli_query($dbx, $sql);
    $typeOfImpact_array = mysqli_fetch_all($typeOfImpact_result);
    
    // Access al fateOfCrew of the LW from database for form input
    $sql = "SELECT id, type FROM fateofcrew";
    $fateOfCrew_result = mysqli_query($dbx, $sql);
    $fateOfCrew_array = mysqli_fetch_all($fateOfCrew_result);
    
    // Load claims from database and add them to the form
    $sql = "SELECT claimslw.id, claimslw.aeroplane, claimslw.claimtime, claimslw.place, claimslw.opponent, ".
           "claimslw.typeofdestr, claimslw.typeofimpact, claimslw.fateofcrew, claimslw.witness AS witnessID, ".
           "acgmembers.callsign AS witness FROM claimslw ".
           "LEFT JOIN acgmembers ON claimslw.witness = acgmembers.id ".
           "WHERE reportID = $r_id";
//    echo($sql);
    $claim_result = mysqli_query($dbx, $sql);
    
    // Load ground claims from database and add them to the form
    $sql = "SELECT claimsground.id, claimsground.object, claimsground.amount, ".
           "claimsground.description, claimsground.accepted FROM claimsground ".
           "WHERE reportID = $r_id";
    $groundclaim_result = mysqli_query($dbx, $sql);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<script type="text/javascript">

var counter = 0;
var groundCounter = 0;
var typeOfDestrArray = <?php echo json_encode($typeOfDestr_array); ?>;
var typeOfImpactArray = <?php echo json_encode($typeOfImpact_array); ?>;
var fateOfCrewArray = <?php echo json_encode($fateOfCrew_array); ?>;
var aeroplane = <?php echo json_encode($aeroplane_claimable_array); ?>;
var groundTargets = <?php echo json_encode($groundTarget_array); ?>;

function addClaim(divName){
    counter = addLWClaim(divName, "", counter, aeroplane, "", "", "", "", 
                         typeOfDestrArray, "", typeOfImpactArray, "",
                         fateOfCrewArray, "", -2, "");
}

function addGClaim(divName){
    groundCounter = addGroundClaim(divName, "", groundCounter, groundTargets,
                                   "", "", 1);
}

function submitReport(){

    gebid("submitStatus").innerHTML = "";
    var r_id = submitLWReport(<?php echo($user_id.', '.$r_id.', '.$c_id.', "'.$firstName.'", "'.$lastName.'"'); ?>);
    var nodelist = document.getElementsByClassName("claim");
    for(n = 0; n < nodelist.length; n++){
        var id = nodelist[n].id;
        submitLWClaim(r_id, <?php echo($c_id); ?>, id);
    }
    var nodelist = document.getElementsByClassName("groundclaims");
    for(n = 0; n < nodelist.length; n++){
        var id = nodelist[n].id;
        submitGroundClaim(r_id, <?php echo($c_id); ?>, id);
    }
    if(r_id > 0){
        window.location = "createReportLW.php?r_id="+r_id;
    }
}

window.onload = function(){
    gebid("submitBtn").addEventListener("click", submitReport, false);
    <?php while($row = mysqli_fetch_assoc($claim_result)) { 
        $witness_id = "";
        if(is_null($row["witnessID"])){$witness_id = "''";}else{$witness_id = $row["witnessID"];}
    ?>

            counter = addLWClaim('claims', <?php echo($row["id"]);?>, counter,
                                aeroplane, <?php echo($row["aeroplane"]);?>, 
                                '<?php echo($row["claimtime"]);?>', '<?php echo($row["opponent"]);?>' , '<?php echo($row["place"]);?>',
                                typeOfDestrArray, <?php echo($row["typeofdestr"]);?>,
                                typeOfImpactArray, <?php echo($row["typeofimpact"]); ?>,
                                fateOfCrewArray, <?php echo($row["fateofcrew"]); ?>,
                                <?php echo $witness_id ?>,
                                '<?php echo($row["witness"]); ?>');
            checkWitness("c"+counter);
    <?php } 
        while($row = mysqli_fetch_assoc($groundclaim_result)) { 
    ?>

            groundCounter = addGroundClaim('groundclaims', <?php echo($row["id"]);?>, 
                                           groundCounter, groundTargets,
                                           <?php echo($row["object"]);?>,
                                           '<?php echo preg_replace("/\r?\n/", "\\n", addslashes($row["description"]));?>',
                                           <?php echo($row["amount"]);?>);
    <?php } ?>
};
</script>   
<?php include(dirname(__FILE__).'/reportMenu.php'); ?> 

<p class="form_id">ACG-PAM/300-210.1</p>
<h3>After Action Report:</h3>
<form id="afterActionReportLW" onsubmit="return false;" >

    <div>
        <span class="AARSpanLeft">Mission date:</span>
        <span class="AARSpanRight"><?php echo $mission_hdate; ?></span>
    </div>

    <div>
        <span class="AARSpanLeft">Name:</span>
        <span class="AARSpanRight"><?php echo $firstName." '".$callsign."' ".$lastName; ?></span>
    </div>

    <div>
        <span class="AARSpanFlightLeft">Swarm:</span>
        <span class="AARSpanFlightRight">
            <select id="swarm" name="swarm">
                <?php createSelectOptions($swarms_result, $swarm); ?>
            </select>
        </span>
        <span class="AARSpanFlightPosLeft">Swarm Pos.:</span>
        <span class="AARSpanFlightPosRight">
            <select id="swarmPos" name="swarmPos">
                <?php createSelectOptions($swarmPos_result, $swarmPos); ?>
            </select>
        </span>
    </div> 
    <div>
        <span class="AARSpanLeft">Type:</span>
        <span class="AARSpanRight">
            <select id="actType" name="actType">
                <?php createSelectOptions($aeroplane_flyable_result, $aeroplane); ?>
            </select>
        </span>
    </div>

    <div>
        <span class="AARSpanLeft">Markings:</span>
        <span class="AARSpanRight"><input type="text" id="markings" name="markings" value="<?php echo $markings ?>"></span>
    </div>

    <div>
        <span class="AARSpanLeft">Aerodrome:</span>
        <span class="AARSpanRight"><input type="text" id="base" name="base" value="<?php echo $aerodrome ?>"></span>
    </div>

    <div>
        <span class="AARSpanLeft">Pilot status:</span>
        <span class="AARSpanRight">
            <select id="pilotStatus" name="pilotStatus">
                <?php createSelectOptions($pilotStatus_result, $pilotStatus); ?>
            </select>
        </span>
    </div>

    <div>
        <span class="AARSpanLeft">Aircraft status:</span>
        <span class="AARSpanRight">
            <select id="acftStatus" name="acftStatus">
                <?php createSelectOptions($aeroplaneStatus_result, $aeroplaneStatus); ?>
            </select>
        </span>
    </div>

    <div id="claims">
        Claims:<br>
        (Checking "Ground Confirmation" will override any other witness information)<br>
    </div>

    <button id="addClaimBtn" onclick="addClaim('claims')">Add claim</button>
    
    <div id="groundclaims">
        Ground claims:<br>
    </div>

    <button id="addGroundClaimBtn" onclick="addGClaim('groundclaims')">Add ground claim</button>

    <div>
        Synopsis (Optional): Images can be included by using [img]image-url[/img].<br>
        <textarea id="synopsis" rows="25" cols="50" maxlength="15000"><?php echo $synopsis ?></textarea>
    </div>

    <div id="submitStatus">&nbsp;</div>
    <button id="submitBtn" >Submit report</button>
</form>
<?php include(dirname(__FILE__).'/footer.php');